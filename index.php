<?php

require __DIR__ . "/vendor/autoload.php";

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);

$dotenv->load();

db()->autoConnect();

app()->post('/', 'App\Controllers\WeatherController@index');

app()->get('/statistics/view/{id}', 'App\Controllers\StatisticsController@view');

app()->run();