<?php

namespace App\ApiHandlers;
use Leaf\Fetch;

class OpenWeatherApi {

    private $latitude;
    private $longitude;
    private $response;
    private array $headers;

    public function __construct($latitude, $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->headers = ['headers' => ['Accept' => 'application/json']];
    }

    public function makeRequest()
    {
        $this->response = Fetch::get('https://api.openweathermap.org/data/3.0/onecall?lat='.$this->latitude.'&lon='.$this->longitude.'&appid='.getenv('OPENWEATHER_API'), $this->headers);

        if(!isset($this->response->data->cod)) {
            return $this->response->data;
        }

        return null;
    }

}