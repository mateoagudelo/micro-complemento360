<?php

namespace App\Repositories;

interface RepositoryInterface
{
    public static function create(array $data);
    public static function find(int $id);
}