<?php

namespace App\Repositories;

class StatisticsRepository implements RepositoryInterface
{

    public static function create(array $data)
    {
        // TODO: Implement create() method.
        db()
            ->insert("statistics")
            ->params([
                "city_name" => $data['city_name'],
                "humidity" => $data['humidity'],
                "created_at" => date('Y-m-d')
            ])
            ->execute();

        return self::find();
    }

    public static function find(int|null $id = null)
    {
        // TODO: Implement find() method.
        $to_query = (is_null($id)) ? db()->lastInsertId() : $id;

        return db()
            ->select('statistics')
            ->where(['id' => $to_query])
            ->first();
    }
}