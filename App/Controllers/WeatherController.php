<?php

namespace App\Controllers;

use App\ApiHandlers\OpenWeatherApi;
use App\Repositories\StatisticsRepository;

class WeatherController
{
    public function index()
    {
        $response = new OpenWeatherApi(request()->get('lat'), request()->get('lon'));
        $response = $response->makeRequest();

        if(!is_null($response)) {
            $last = StatisticsRepository::create(['city_name' => 'Miami', 'humidity' => $response->current->humidity]);
            return response()->json([$last]);
        }

        return response()->json(['message' => 'Not found'], '404');
    }
}