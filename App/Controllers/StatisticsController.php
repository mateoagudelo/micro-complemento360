<?php

namespace App\Controllers;

use App\Repositories\StatisticsRepository;

class StatisticsController
{
    public function view(int $id)
    {
        if($statistic = StatisticsRepository::find($id)) {
            return response()->json($statistic);
        }

        return response()->json(['message' => 'Not found'], '404');
    }
}